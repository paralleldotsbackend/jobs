import feedparser
import json
import requests
import datetime
from pymongo import MongoClient
from pyquery import PyQuery as pq
from dateutil.parser import parse
from bson.objectid import ObjectId

jobs_data = MongoClient("mongodb://104.155.210.134/jobs",27017)["jobs"]["jobs_doc"]

def updatePastDays():
	jobs = jobs_data.find()
	for obj in jobs:
		job_link = obj.get('job_link')
		a  = requests.get(str(job_link))
		dd = pq(a.text)
		job_passed_days = dd('.date').eq(0).text()
		jobs_data.update({'_id': ObjectId(str(obj.get('_id'))) },{'$set':{'job_passed_days': job_passed_days}})
	return 1

print updatePastDays()