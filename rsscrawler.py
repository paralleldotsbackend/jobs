import feedparser
import json
import requests
import datetime
from pymongo import MongoClient
from pyquery import PyQuery as pq
from dateutil.parser import parse
from bson.objectid import ObjectId
from elasticsearch import Elasticsearch
import pika
import traceback
# from pybloom import ScalableBloomFilter
# sbf = ScalableBloomFilter(mode=ScalableBloomFilter.SMALL_SET_GROWTH)

# jobs_data = MongoClient("mongodb://104.155.210.134/jobs",27017)["jobs"]["jobs_doc"]
# es = Elasticsearch([{'host': '104.154.89.51', 'port': 9200}])

# parameters1     = pika.URLParameters('amqp://ankit:root53@146.148.71.201:5672/dev')
parameters1 = pika.URLParameters('amqp://ankit:root53@146.148.71.201:5672/%2F')
connection1     = pika.BlockingConnection(parameters=parameters1)
channel1 = connection1.channel()
channel1.queue_declare(queue='jobs_bloomfilter', durable=True)
  
def jobsCrawling():
	cities_list    = ['gurgaon','gurugram','delhi','noida','mumbai','pune','bangalore','hyderabad','chennai']
	developer_list = ['ruby on rails','ruby developer','android developer','python developer','java developer','ios developer','.net developer','php developer','javascript developer','cto','chief technology officer','tech lead','c++ developer','c developer','business development','growth hacker','c# developer','mongodb','mysql','node.js','content writer','backend developer','front-end developer','designer','user interface','user experience','devops','data science','machine learning','unix']

	for k in range(0,len(cities_list)):
		for l in range(0,len(developer_list)):
			d = feedparser.parse('http://www.indeed.co.in/rss?q='+developer_list[l]+'&l='+cities_list[k]+'')
			for i in range(0,len(d['entries'])):
				try:
					temp  = {}
				
					print "=========================================================================="
					job_link                    =  d['entries'][i]['link']
					job_id                      =  d['entries'][i]['id']
					
					job_published_date          =  parse(d['entries'][i]['published'])
					
					job_title                   =  d['entries'][i]['title']
					job_summary                 =  d['entries'][i]['summary']
					job_source                  =  d['entries'][i]['source']['title']
					
					a                           =  requests.get(str(d['entries'][i]['link']))
					dd                          =  pq(a.text)
					
					job_title_main              =  dd('.jobtitle').eq(0).text()
					job_location                =  dd('#job-content .location').eq(0).text()
					job_company                 =  dd('#job-content .company').eq(0).text()
					job_summary_main            =  dd('#job_summary').eq(0).text()
					job_passed_days             =  dd('.date').eq(0).text() 
					
					temp['job_link']            =  job_link
					temp['job_id']              =  job_id
					temp['job_published_date']  =  str(job_published_date)
					temp['job_title']           =  job_title
					temp['job_summary']         =  job_summary
					temp['job_source']          =  job_source
					temp['job_title_main']      =  job_title_main
					temp['job_location']        =  job_location
					temp['job_company']         =  job_company
					temp['job_summary_main']    =  job_summary_main
					temp['job_passed_days']     =  job_passed_days
					temp['job_type']            =  developer_list[l]
					temp['job_city']            =  cities_list[k]
					temp['created_at']          =  str(datetime.datetime.now())

					temp = json.dumps(temp)

					try:
						channel1.basic_publish(exchange='',
							routing_key='jobs_bloomfilter',
							body=temp,
							properties=pika.BasicProperties(
							delivery_mode = 2, # make message persistent
							))
					except Exception,e:
						print e
						print "Error in queeeue"
						print "==========================================================================="
				except Exception, e:
					print e
					print "EXCEPTION"
					continue

jobsCrawling()