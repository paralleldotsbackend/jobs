import pika
from pybloom import ScalableBloomFilter
import json
#Initializing Bloom Filter
sbf = ScalableBloomFilter(mode=ScalableBloomFilter.SMALL_SET_GROWTH)


#RabbitMQ Stuff
parameters = pika.URLParameters('amqp://ankit:root53@146.148.71.201:5672/%2F')
connection = pika.BlockingConnection(parameters=parameters)
channel1   = connection.channel()
channel2   = connection.channel()
channel1.queue_declare(queue='jobs_bloomfilter', durable=True)
channel2.queue_declare(queue='jobs_crawler', durable=True)
print(' [*] Waiting for messages. To exit press CTRL+C')

def callback(ch, method, properties, body):
	print(" [x] Received %r" % body)
	job_id = json.loads(body)['job_id']
	if not sbf.add(job_id):
		channel2.basic_publish(exchange='',
										routing_key='jobs_crawler',
										body=body,
										properties=pika.BasicProperties(
											 delivery_mode = 2, # make message persistent
										))

	ch.basic_ack(delivery_tag = method.delivery_tag)


channel1.basic_qos(prefetch_count=1)
channel1.basic_consume(callback,queue='jobs_bloomfilter')
channel1.start_consuming()