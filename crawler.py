from pymongo import MongoClient
from pyquery import PyQuery as pq
from dateutil.parser import parse
from bson.objectid import ObjectId
from elasticsearch import Elasticsearch
import pika
import json
import traceback
import sys
import datetime

jobs_data = MongoClient("mongodev.paralleldots.com")["jobs"]["jobs_doc"]
es = Elasticsearch([{'host': '104.154.89.51', 'port': 9200}])

parameters = pika.URLParameters('amqp://ankit:root53@146.148.71.201:5672/%2F')
connection = pika.BlockingConnection(parameters=parameters)
channel1 = connection.channel()
channel1.queue_declare(queue='jobs_crawler', durable=True)

def callback(ch, method, properties, body):
	data = json.loads(body.decode('utf-8'))
	try:
		temp1 = {}
		temp1['job_link']            =  data.get('job_link')
		temp1['job_id']              =  data.get('job_id')
		job_published_date           =  datetime.datetime.strptime(str(data.get('job_published_date')).split('+')[0],"%Y-%m-%d %H:%M:%S")
		temp1['job_published_date']  =  job_published_date
		temp1['job_title']           =  data.get('job_title')
		temp1['job_summary']         =  data.get('job_summary')
		temp1['job_source']          =  data.get('job_source')
		temp1['job_title_main']      =  data.get('job_title_main')
		temp1['job_location']        =  data.get('job_location')
		temp1['job_company']         =  data.get('job_company')
		temp1['job_summary_main']    =  data.get('job_summary_main')
		temp1['job_passed_days']     =  data.get('job_passed_days')
		temp1['job_type']            =  data.get('job_type')
		temp1['job_city']            =  data.get('job_city')
		temp1['created_at']          =  datetime.datetime.now()
		id_ob = jobs_data.insert(data)
		# print "-----------------"
		# print temp1
		res = es.index(index="nasscom_jobs_search",doc_type='nasscom_jobs_tracking',id=str(id_ob),body=dict(temp1))
		
	except:
		print traceback.format_exc()
	ch.basic_ack(delivery_tag = method.delivery_tag)
channel1.basic_qos(prefetch_count=1)
channel1.basic_consume(callback,queue='jobs_crawler')
print(' [*] Waiting for messages. To exit press CTRL+C')
channel1.start_consuming()