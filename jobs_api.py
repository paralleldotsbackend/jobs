import os
import json
import time
import calendar
import datetime
from tornado import ioloop,web
import urllib
import redis,ast
import hashlib
from memsql.common import database
import MySQLdb
import ast
import operator
from pymongo import MongoClient
from elasticsearch import Elasticsearch
from dateutil.parser import parse

es = Elasticsearch([{'host': '104.154.89.51', 'port': 9200}])
jobs_data = MongoClient("mongodev.paralleldots.com")["jobs"]["jobs_doc"]

location_dict = {"noida":"Noida, Uttar Pradesh","delhi":"New Delhi, Delhi","chennai":"Chennai, Tamil Nadu","hyderabad":"Hyderabad, Andhra Pradesh","pune":"Pune, Maharashtra","bangalore":"Bangalore, Karnataka","mumbai":"Mumbai, Maharashtra"}

class NasscomJobsHandler(web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        city    = self.get_argument('city',None)
        page    = self.get_argument('page',None)
        keyword = self.get_argument('keyword',None) 
        
        if page is None:
          self.set_header("Content-Type", "application/json")
          self.set_status(200)
          self.finish(json.dumps({'Status':0,"msg":"Please enter page number"}))
          return
        
        # if keyword is None:
        #   self.set_header("Content-Type", "application/json")
        #   self.set_status(200)
        #   self.finish(json.dumps({'Status':0,"msg":"Please provide keyword"}))
        #   return

        if int(page) > 100:
          self.set_header("Content-Type", "application/json")
          self.set_status(200)
          self.finish(json.dumps({'Status':1,"msg":"There is a limit of from 1 to 100 pages in this api"}))
          return  
        
        if int(page) >= 1 and int(page) <= 100:  
          enddate=datetime.datetime.now()
          startdate=enddate-datetime.timedelta(days=15)
          end_date_str=enddate.strftime("%Y-%m-%d %H:%M:%S")
          start_date_str=startdate.strftime("%Y-%m-%d %H:%M:%S")
          
          from_page = int(page)*10 - 10
          # to_page   = int(page)*10

          # print from_page
          # print to_page

          if city is not None:
              if keyword is None:
                if city == "chennai":
                    chennai = []
                    # city = ["gurgaon","gurugram"]
                    city = location_dict[str(city)]
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                      
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                        }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)
                    print len(data)
                    if data is not None:
                        for obj in data['hits']['hits']:
                            chennai.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'chennai'})
                        
                        chennai = sorted(chennai, key=operator.itemgetter('published_date'),reverse=True)

                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'chennai':chennai}}))
                    return

                if city == "delhi":
                    city = "delhi"
                    city = location_dict[str(city)]
                    delhi = []
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)

                    if data is not None:
                        for obj in data['hits']['hits']:
                            delhi.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'delhi'})    
                        
                        
                        delhi = sorted(delhi, key=operator.itemgetter('published_date'),reverse=True)

                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'delhi':delhi}}))
                    return

                elif city == "noida":
                    city = "noida"
                    city = location_dict[str(city)]
                    noida = []
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                
                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)
                    if data is not None:
                        for obj in data['hits']['hits']:
                            noida.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'noida'})
                        
                        noida = sorted(noida, key=operator.itemgetter('published_date'),reverse=True)
                    
                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'noida':noida}}))
                    return

                elif city == "bangalore":
                    city      = "bangalore"
                    city = location_dict[str(city)]
                    bangalore = []
                    print "---------------------"
                    print from_page
                    # print to_page
                    print "---------------------"
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)
                    print "****************************************"
                    print len(data['hits']['hits'])
                    print "*****************************************"
                    if data is not None:
                        for obj in data['hits']['hits']:
                            bangalore.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'bangalore'})
                    
                        bangalore = sorted(bangalore, key=operator.itemgetter('published_date'),reverse=True)


                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'bangalore':bangalore}}))
                    return
                
                elif city == "pune":
                    city = "pune"
                    city = location_dict[str(city)]
                    pune = []
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)

                    if data is not None:
                        for obj in data['hits']['hits']:
                            pune.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'pune'})
                    
                        pune = sorted(pune, key=operator.itemgetter('published_date'),reverse=True)

                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'pune':pune}}))
                    return
                
                elif city == "mumbai":
                    city = "mumbai"
                    city = location_dict[str(city)]
                    mumbai = []

                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)

                    if data is not None:
                        for obj in data['hits']['hits']:
                            mumbai.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'mumbai'})
                    
                        mumbai = sorted(mumbai, key=operator.itemgetter('published_date'),reverse=True)
                    
                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'mumbai':mumbai}}))
                    return

                elif city == "hyderabad":
                    city = "hyderabad"
                    city = location_dict[str(city)]
                    hyderabad = []
                    print "========================================"
                    print from_page
                    # print to_page
                    print "================================================"
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)

                    if data is not None: 

                        for obj in data['hits']['hits']:
                            hyderabad.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'hyderabad'})
                    
                        hyderabad = sorted(hyderabad, key=operator.itemgetter('published_date'),reverse=True)

                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'hyderabad':hyderabad}}))
                    return

                else:
                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':[],"msg":"Please enter correct spelling or we are not crawling this city jobs"}))
                    return
                # print "-------------"

              else:  
                if city == "chennai":
                    chennai = []
                    # city = ["gurgaon","gurugram"]
                    city = location_dict[str(city)]
                    
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                      
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }},{"multi_match" : {"query": keyword, "type": "phrase_prefix", "fields": ["job_summary_main^2", "job_title_main" ]}}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                        }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)
                    print len(data)
                    if data is not None:
                        for obj in data['hits']['hits']:
                            chennai.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'chennai'})
                        
                        chennai = sorted(chennai, key=operator.itemgetter('published_date'),reverse=True)

                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'chennai':chennai}}))
                    return

                if city == "delhi":
                    city = "delhi"
                    city = location_dict[str(city)]
                    delhi = []
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }},{"multi_match" : {"query": keyword, "type": "phrase_prefix", "fields": ["job_summary_main^2", "job_title_main" ]}}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)

                    if data is not None:
                        for obj in data['hits']['hits']:
                            delhi.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'delhi'})    
                        
                        
                        delhi = sorted(delhi, key=operator.itemgetter('published_date'),reverse=True)

                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'delhi':delhi}}))
                    return

                elif city == "noida":
                    city = "noida"
                    city = location_dict[str(city)]
                    noida = []
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }},{"multi_match" : {"query": keyword, "type": "phrase_prefix", "fields": ["job_summary_main^2", "job_title_main" ]}}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                
                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)
                    if data is not None:
                        for obj in data['hits']['hits']:
                            noida.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'noida'})
                        
                        noida = sorted(noida, key=operator.itemgetter('published_date'),reverse=True)
                    
                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'noida':noida}}))
                    return

                elif city == "bangalore":
                    city      = "bangalore"
                    city = location_dict[str(city)]
                    bangalore = []
                    print "---------------------"
                    print from_page
                    # print to_page
                    print "---------------------"
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }},{"multi_match" : {"query": keyword, "type": "phrase_prefix", "fields": ["job_summary_main^2", "job_title_main" ]}}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)
                    print "****************************************"
                    print len(data['hits']['hits'])
                    print "*****************************************"
                    if data is not None:
                        for obj in data['hits']['hits']:
                            bangalore.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'bangalore'})
                    
                        bangalore = sorted(bangalore, key=operator.itemgetter('published_date'),reverse=True)


                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'bangalore':bangalore}}))
                    return
                
                elif city == "pune":
                    city = "pune"
                    city = location_dict[str(city)]
                    pune = []
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }},{"term" : {"job_type" : str(keyword)}}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)

                    if data is not None:
                        for obj in data['hits']['hits']:
                            pune.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'pune'})
                    
                        pune = sorted(pune, key=operator.itemgetter('published_date'),reverse=True)

                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'pune':pune}}))
                    return
                
                elif city == "mumbai":
                    city = "mumbai"
                    city = location_dict[str(city)]
                    mumbai = []

                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }},{"multi_match" : {"query": keyword, "type": "phrase_prefix", "fields": ["job_summary_main^2", "job_title_main" ]}}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)

                    if data is not None:
                        for obj in data['hits']['hits']:
                            mumbai.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'mumbai'})
                    
                        mumbai = sorted(mumbai, key=operator.itemgetter('published_date'),reverse=True)
                    
                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'mumbai':mumbai}}))
                    return

                elif city == "hyderabad":
                    city = "hyderabad"
                    city = location_dict[str(city)]
                    hyderabad = []
                    print "========================================"
                    print from_page
                    # print to_page
                    print "================================================"
                    q   =   {
                      'from':from_page,'size' : 10,
                      "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"term": {"job_location": city }},{"multi_match" : {"query": keyword, "type": "phrase_prefix", "fields": ["job_summary_main^2", "job_title_main" ]}}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                       }
                      }
                      
                

                    data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)

                    if data is not None: 

                        for obj in data['hits']['hits']:
                            hyderabad.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'hyderabad'})
                    
                        hyderabad = sorted(hyderabad, key=operator.itemgetter('published_date'),reverse=True)

                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':{'hyderabad':hyderabad}}))
                    return

                else:
                    self.set_header("Content-Type", "application/json")
                    self.set_status(200)
                    self.finish(json.dumps({'Status':1,'data':[],"msg":"Please enter correct spelling or we are not crawling this city jobs"}))
                    return

          else:
              if keyword is None:    
                cities_list    = [location_dict['chennai'],location_dict['delhi'],location_dict['noida'],location_dict['mumbai'],location_dict['pune'],location_dict['bangalore'],location_dict['hyderabad']]
                q   =   {
                        'from':from_page,'size':10,
                        "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"terms": {"job_location": cities_list }}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                        
                       }
                      }
                      

                data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)
                chennai = []
                delhi = []
                noida = []
                mumbai = []
                pune = []
                bangalore = []
                hyderabad = []

                for obj in data['hits']['hits']:
                    if obj['_source']['job_city'] == "chennai":
                        chennai.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'chennai'})
                    
                    elif obj['_source']['job_city'] == "delhi":
                        delhi.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'delhi'})
                    
                    elif obj['_source']['job_city'] == "noida":
                        noida.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'noida'})   
                    
                    elif obj['_source']['job_city'] == "mumbai":
                        mumbai.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'mumbai'})  
                    
                    elif obj['_source']['job_city'] == "pune":
                        pune.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'pune'}) 
                    
                    elif obj['_source']['job_city'] == "bangalore":
                        bangalore.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'bangalore'})
                    
                    elif obj['_source']['job_city'] == "hyderabad":
                        hyderabad.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'hyderabad'})

                bangalore = sorted(bangalore, key=operator.itemgetter('published_date'),reverse=True)
                chennai   = sorted(chennai, key=operator.itemgetter('published_date'),reverse=True)
                delhi     = sorted(delhi, key=operator.itemgetter('published_date'),reverse=True)
                noida     = sorted(noida, key=operator.itemgetter('published_date'),reverse=True)
                mumbai    = sorted(mumbai, key=operator.itemgetter('published_date'),reverse=True)
                pune      = sorted(pune, key=operator.itemgetter('published_date'),reverse=True)
                hyderabad = sorted(hyderabad, key=operator.itemgetter('published_date'),reverse=True)

                self.set_header("Content-Type", "application/json")
                self.set_status(200)
                self.finish(json.dumps({'Status':1,'data':{'chennai':chennai,'delhi':delhi,'noida':noida,'hyderabad':hyderabad,'pune':pune,'bangalore':bangalore,'mumbai':mumbai}}))
                return                                
              else:
                cities_list    = [location_dict['chennai'],location_dict['delhi'],location_dict['noida'],location_dict['mumbai'],location_dict['pune'],location_dict['bangalore'],location_dict['hyderabad']]
                q   =   {
                        'from':from_page,'size':10,
                        "sort" : [
                                  { "job_published_date" : {"order" : "desc"}},
            
                                ],
                        'query': {
                        'filtered': {
                        'query': {
                        "bool": {
                              "must": [{"terms": {"job_location": cities_list }},{"multi_match" : {"query": keyword, "type": "phrase_prefix", "fields": ["job_summary_main^2", "job_title_main" ]}}]
                          }
                          },
                        "filter":{
                        "range":{
                        'job_published_date':{'gte':startdate,'lte':enddate
                         }
                        }
                        }
                         }
                        
                       }
                      }
                      

                data = es.search(index='nasscom_jobs_search',doc_type='nasscom_jobs_tracking',body=q)
                chennai = []
                delhi = []
                noida = []
                mumbai = []
                pune = []
                bangalore = []
                hyderabad = []

                for obj in data['hits']['hits']:
                    if obj['_source']['job_city'] == "chennai":
                        chennai.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'chennai'})
                    
                    elif obj['_source']['job_city'] == "delhi":
                        delhi.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'delhi'})
                    
                    elif obj['_source']['job_city'] == "noida":
                        noida.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'noida'})   
                    
                    elif obj['_source']['job_city'] == "mumbai":
                        mumbai.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'mumbai'})  
                    
                    elif obj['_source']['job_city'] == "pune":
                        pune.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'pune'}) 
                    
                    elif obj['_source']['job_city'] == "bangalore":
                        bangalore.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'bangalore'})
                    
                    elif obj['_source']['job_city'] == "hyderabad":
                        hyderabad.append({'job_link':obj['_source'].get('job_link'),'published_date':obj['_source'].get('job_published_date'),'job_id':obj['_source'].get('job_id'),'job_title':obj['_source'].get('job_title'),'job_summary':obj['_source'].get('job_summary'),'job_source':obj['_source'].get('job_source'),'job_title_main':obj['_source'].get('job_title_main'),'job_location':obj['_source'].get('job_location'),'job_company':obj['_source'].get('job_company'),'job_summary_main':obj['_source'].get('job_summary_main'),'job_type':obj['_source'].get('job_type'),'job_city':'hyderabad'})

                bangalore = sorted(bangalore, key=operator.itemgetter('published_date'),reverse=True)
                chennai   = sorted(chennai, key=operator.itemgetter('published_date'),reverse=True)
                delhi     = sorted(delhi, key=operator.itemgetter('published_date'),reverse=True)
                noida     = sorted(noida, key=operator.itemgetter('published_date'),reverse=True)
                mumbai    = sorted(mumbai, key=operator.itemgetter('published_date'),reverse=True)
                pune      = sorted(pune, key=operator.itemgetter('published_date'),reverse=True)
                hyderabad = sorted(hyderabad, key=operator.itemgetter('published_date'),reverse=True)

                self.set_header("Content-Type", "application/json")
                self.set_status(200)
                self.finish(json.dumps({'Status':1,'data':{'chennai':chennai,'delhi':delhi,'noida':noida,'hyderabad':hyderabad,'pune':pune,'bangalore':bangalore,'mumbai':mumbai}}))
                return
        else:
          self.set_header("Content-Type", "application/json")
          self.set_status(200)
          self.finish(json.dumps({'Status':1,"msg":"There is a limit of from 1 to 100 pages in this api"}))
          return      
settings = {
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
    "debug" : False
}
 
application = web.Application([
    (r'/nasscom/api/jobs', NasscomJobsHandler),
],**settings)

if __name__ == "__main__":
    print "Here we go"
    application.listen(9191)
    ioloop.IOLoop.instance().start()

